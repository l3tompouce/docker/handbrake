FROM alpine:20221110
LABEL maintainer "ToM <tom@leloop.org>"

ARG HANDBRAKE_VERSION=1.6.0-r0

WORKDIR /files
ENTRYPOINT ["HandBrakeCLI"]

RUN echo '@testing http://dl-cdn.alpinelinux.org/alpine/edge/testing' >> /etc/apk/repositories \
    && apk --no-cache add handbrake@testing=${HANDBRAKE_VERSION}
