# letompouce/handbrake

Docker image for [HandBrake](https://handbrake.fr/) CLI.

## Run

```shell
docker run --rm \
  -v $(pwd):/files \
  letompouce/handbrake \
  --preset-list
```

This image uses UTC timezone, you may want to mount
`/etc/localtime:/etc/localtime:ro`.

## Docker Tags

### Simple tags

* `a1b2c3d4`: CI build at `$CI_COMMIT_SHORT_SHA`
* `v1.3.1-r1`: CI build at `$CI_COMMIT_TAG`. Reflects the HandBrake version.

### Shared tags

* `latest`: Points to latest `vX.X` release tag. Stable image that should be
  used in production.
* `head`: points to latest `push` build. May not be production-ready.

## Volumes

* `/files` is the image WORKDIR.

## Todo

* Custom presets import
